//
//  DictionaryofC.swift
//  VOCofT
//
//  Created by apple on 2017/5/26.
//  Copyright © 2017年 LuHongyuan. All rights reserved.
//

import Foundation

class DictionaryofC {
    var englishvoc = ""
    var TImages = ""
    var chinese = ""
    var kkphonetic = ""
    var isKnowed = false
    
    init(englishvoc: String, TImages: String, chinese: String, kkphonetic: String, isKnowned: Bool) {
        self.englishvoc = englishvoc
        self.TImages = TImages
        self.chinese = chinese
        self.kkphonetic = kkphonetic
        self.isKnowed = isKnowned
    }
}
