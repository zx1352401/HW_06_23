//
//  VOCTableViewController.swift
//  VOCofT
//
//  Created by LuHongyuan on 2017/4/21.
//  Copyright © 2017年 LuHongyuan. All rights reserved.
//

import UIKit
import AVFoundation

class VOCTableViewController: UITableViewController {
    
    var dictionaryofC : [DictionaryofC] = [
        DictionaryofC(englishvoc: "tablespoon", TImages: "tablespoon.jpg", chinese: "大湯匙", kkphonetic: "tābəlˌspo͞on", isKnowned: false),
        DictionaryofC(englishvoc: "tablet", TImages: "tablet.jpg", chinese: "平板", kkphonetic: "ˈtablət", isKnowned: false),
        DictionaryofC(englishvoc: "tailor", TImages: "tailor.jpg", chinese: "裁縫師", kkphonetic: "ˈtālər", isKnowned: false),
        DictionaryofC(englishvoc: "tale", TImages: "tale.jpg", chinese: "故事", kkphonetic: "tāl", isKnowned: false),
        DictionaryofC(englishvoc: "tangerine", TImages: "tangerine.jpg", chinese: "橘子", kkphonetic: "ˌtanjəˈrēn", isKnowned: false),
        DictionaryofC(englishvoc: "tanker", TImages: "tanker.jpg", chinese: "油輪", kkphonetic: "ˈtaNGkər", isKnowned: false),
        DictionaryofC(englishvoc: "technician", TImages: "technician.jpg", chinese: "技術人員", kkphonetic: "tekˈniSHən", isKnowned: false),
        DictionaryofC(englishvoc: "teenager", TImages: "teenager.jpg", chinese: "青少年", kkphonetic: "ˈtēnˌājər", isKnowned: false),
        DictionaryofC(englishvoc: "telegram", TImages: "telegram.jpg", chinese: "電報", kkphonetic: "telegram", isKnowned: false),
        DictionaryofC(englishvoc: "telescope", TImages: "telescope.jpg", chinese: "望遠鏡", kkphonetic: "ˈteləˌskōp", isKnowned: false),
        DictionaryofC(englishvoc: "television", TImages: "television.jpg", chinese: "電視", kkphonetic: "ˈteləˌviZHən", isKnowned: false),
        DictionaryofC(englishvoc: "temperature", TImages: "temperature.jpg", chinese: "溫度", kkphonetic: "ˈtemp(ə)rəCHər", isKnowned: false),
        DictionaryofC(englishvoc: "throat", TImages: "throat.jpg", chinese: "喉嚨", kkphonetic: "THrōt", isKnowned: false),
        DictionaryofC(englishvoc: "thunderstorm", TImages: "thunderstorm.jpg", chinese: "大雷雨", kkphonetic: "THəndərˌstôrm", isKnowned: false),
        DictionaryofC(englishvoc: "tissue", TImages: "tissue.jpg", chinese: "衛生紙", kkphonetic: "tiSHo͞o", isKnowned: false),
        DictionaryofC(englishvoc: "tourist", TImages: "tourist.jpg", chinese: "觀光客", kkphonetic: "ˈto͝orəst", isKnowned: false),
        DictionaryofC(englishvoc: "trademark", TImages: "trademark.jpg", chinese: "商標", kkphonetic: "ˈtrādˌmärk", isKnowned: false),
        DictionaryofC(englishvoc: "typhoon", TImages: "typhoon.jpg", chinese: "颱風", kkphonetic: "tīˈfo͞on", isKnowned: false),
        DictionaryofC(englishvoc: "tuxedo", TImages: "tuxedo.jpg", chinese: "燕尾服", kkphonetic: "təkˈsēdō", isKnowned: false),
        DictionaryofC(englishvoc: "trumpet", TImages: "trumpet.jpg", chinese: "喇叭", kkphonetic: "ˈtrəmpət", isKnowned: false),
        DictionaryofC(englishvoc: "tyre", TImages: "tyre.jpg", chinese: "輪胎", kkphonetic: "tī(ə)r", isKnowned: false),
        DictionaryofC(englishvoc: "trout", TImages: "trout.jpg", chinese: "鮭魚", kkphonetic: "trout", isKnowned: false),
        DictionaryofC(englishvoc: "trousers", TImages: "trousers.jpg", chinese: "褲子", kkphonetic: "ˈtrouzərz", isKnowned: false),
        DictionaryofC(englishvoc: "topics", TImages: "topics.jpg", chinese: "熱帶", kkphonetic: "ˈtäpik", isKnowned: false),
        DictionaryofC(englishvoc: "trolley", TImages: "trolley.jpg", chinese: "手推車", kkphonetic: "ˈträlē", isKnowned: false),
        DictionaryofC(englishvoc: "tribe", TImages: "tribe.jpg", chinese: "部落", kkphonetic: "trīb", isKnowned: false),
        DictionaryofC(englishvoc: "translator", TImages: "translator.jpg", chinese: "翻譯機", kkphonetic: "ˈtransˌlādər", isKnowned: false),
        DictionaryofC(englishvoc: "transfusion", TImages: "transfusion.jpg", chinese: "輸血", kkphonetic: "ˌtran(t)sˈfyo͞oZHən", isKnowned: false),
        DictionaryofC(englishvoc: "track", TImages: "track.jpg", chinese: "軌道", kkphonetic: "trak", isKnowned: false),
        DictionaryofC(englishvoc: "township", TImages: "township.jpg", chinese: "鄉", kkphonetic: "ˈtounˌSHip", isKnowned: false)
        
    ]

    
//    var arrenglish:Dictionary<String,[String]>=
//        [
//            "englishvoc":    ["tablespoon","tablet","tailor","tale","tangerine","tanker","technician","teenager","telegram","telescope","television","temperature","throat","thunderstorm","tissue","tourist","trademark","typhoon","tuxedo","trumpet","tyre","trout","trousers","topics","trolley","tribe","translator","transfusion","track","township"],
//            "TImages":    ["tablespoon.jpg","tablet.jpg","tailor.jpg","tale.jpg","tangerine.jpg","tanker.jpg","technician.jpg","teenager.jpg","telegram.jpg","telescope.jpg","television.jpg","temperature.jpg","throat.jpg","thunderstorm.jpg","tissue.jpg","tourist.jpg","trademark.jpg","typhoon.jpg","tuxedo.jpg","trumpet.jpg","tyre.jpg","trout.jpg","trousers.jpg","topics.jpg","trolley.jpg","tribe.jpg","translator.jpg","transfusion.jpg","track.jpg","township.jpg"],
//            "chinese":
//                ["大湯匙","平板","裁縫師","故事","橘子","油輪","技術人員","青少年","電報","望遠鏡","電視","溫度","喉嚨","大雷雨","衛生紙","觀光客","商標","颱風","燕尾服","喇叭","輪胎","鮭魚","褲子","熱帶","手推車","部落","翻譯機","輸血","軌道","鄉"],
//            "kkphonetic":
//                ["ˈtābəlˌspo͞on","ˈtablət","ˈtālər","tāl","ˌtanjəˈrēn","ˈtaNGkər","tekˈniSHən","ˈtēnˌājər","telegram","ˈteləˌskōp",
//                 "ˈteləˌviZHən","ˈtemp(ə)rəCHər","THrōt","ˈTHəndərˌstôrm","ˈtiSHo͞o" ,"ˈto͝orəst","ˈtrādˌmärk","tīˈfo͞on",
//                 "təkˈsēdō","ˈtrəmpət","tī(ə)r","trout","ˈtrouzərz","ˈtäpik","ˈträlē","trīb","ˈtransˌlādər","ˌtran(t)sˈfyo͞oZHən","trak",
//                 "ˈtounˌSHip"]
//    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let string = "안녕하세요"
        let synthesizer = AVSpeechSynthesizer()
        let voice = AVSpeechSynthesisVoice(language:"ko-KR")
        let utterance = AVSpeechUtterance(string:string)
        utterance.voice = voice
        synthesizer.speak(utterance)
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return   dictionaryofC.count  }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "outCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! VOCTableViewCell
        
        // Configure the cell...
        cell.VOC.text = dictionaryofC[indexPath.row].englishvoc
        cell.TImagesView.image = UIImage(named:dictionaryofC[indexPath.row].TImages)
        cell.chinese.text = dictionaryofC[indexPath.row].chinese
        cell.KK.text = dictionaryofC[indexPath.row].kkphonetic
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue,sender: Any?){
        if segue.identifier == "showVOCDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! VOCDetailViewController
                destinationController.vocImage = dictionaryofC[indexPath.row].TImages
                destinationController.Englishlabel = dictionaryofC[indexPath.row].englishvoc
                destinationController.chineselabel = dictionaryofC[indexPath.row].chinese
                destinationController.kklabel = dictionaryofC[indexPath.row].kkphonetic
            }
        }
    }
        
    
    
    
    
    
    
    
    
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let optionMenu = UIAlertController(title:nil,message:"What do you want to do?",preferredStyle: .actionSheet)
//        let cancelAction = UIAlertAction(title:"Cancel",style:.cancel,handler:nil)
//        self.present(optionMenu,animated: true,completion: nil)
//        optionMenu.addAction(cancelAction)
//        let callActionHandler = {(action:UIAlertAction!) -> Void in
//            let alertMessage = UIAlertController(title:"NOW Speaking",message:self.arrenglish["englishvoc"]![indexPath.row],preferredStyle:.alert)
//            alertMessage.addAction(UIAlertAction(title:"OK",style:.default,handler:nil))
//            self.present(alertMessage,animated: true,completion: nil)
//            super.viewDidLoad()
//            let string = self.arrenglish["englishvoc"]![indexPath.row]
//            let synthesizer = AVSpeechSynthesizer()
//            let voice = AVSpeechSynthesisVoice(language:"en-US")
//            let utterance = AVSpeechUtterance(string:string)
//            utterance.voice = voice
//            synthesizer.speak(utterance)
//        }
//        let callAction = UIAlertAction(title:"speak  "+arrenglish["englishvoc"]![indexPath.row],style:.default,handler: callActionHandler)
//        optionMenu.addAction(callAction)
//    }
    override func tableView(_ tableView:UITableView,commit editingStyle:UITableViewCellEditingStyle,forRowAt indexPath:IndexPath){
        if editingStyle == .delete{
            dictionaryofC.remove(at: indexPath.row)
//            arrenglish["englishvoc"]?.remove(at:indexPath.row)
//            arrenglish["TImages"]?.remove(at:indexPath.row)
//            arrenglish["chinese"]?.remove(at:indexPath.row)
//            arrenglish["kkphonetic"]?.remove(at:indexPath.row)
        }
        //tableView.deleteRows(at: [indexPath], with: .middle)
    }
    override func tableView(_ tableView:UITableView,editActionsForRowAt indexPath: IndexPath) ->[UITableViewRowAction]?{
        
        let shareAction = UITableViewRowAction(style:UITableViewRowActionStyle.default,title:"SHARE",handler:{
            (action,indexPath) -> Void in
            let defaultText = "Just hecking in at "+self.dictionaryofC[indexPath.row].englishvoc
            let activityController = UIActivityViewController(activityItems:[defaultText],applicationActivities:nil)
            self.present(activityController,animated: true,completion: nil)
        })
        
        
        let deleteAction = UITableViewRowAction(style:UITableViewRowActionStyle.default,title:"DELETE"){
            (action,indexPath) -> Void in
            self.dictionaryofC.remove(at: indexPath.row)
           
            self.tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        let speakAction = UITableViewRowAction(style:UITableViewRowActionStyle.default,title:"SPEAK CHINESE"){
            (action,indexPath) -> Void in
            super.viewDidLoad()
            let string = self.dictionaryofC[indexPath.row].chinese
            let synthesizer = AVSpeechSynthesizer()
            let voice = AVSpeechSynthesisVoice(language:"zh-TW")
            let utterance = AVSpeechUtterance(string:string)
            utterance.voice = voice
            synthesizer.speak(utterance)
        }
        
        
        
        
        
        
        
        return [shareAction,deleteAction,speakAction]
        
    }

    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
