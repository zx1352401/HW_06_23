//
//  VOCDetailViewController.swift
//  VOCofT
//
//  Created by apple on 2017/5/19.
//  Copyright © 2017年 LuHongyuan. All rights reserved.
//

import UIKit
import AVFoundation

class VOCDetailViewController: UIViewController {
    @IBOutlet var vocImageView:UIImageView!
    var vocImage = ""
    @IBOutlet var English2:UILabel!
    var Englishlabel = ""
    @IBOutlet var chinese2:UILabel!
    var chineselabel = ""
    @IBOutlet var KK2:UILabel!
    var kklabel = ""
    
    @IBAction func output(_ sender: UIButton) {
        let string = Englishlabel
        let synthesizer = AVSpeechSynthesizer()
        let voice = AVSpeechSynthesisVoice(language:"en-US")
        let utterance = AVSpeechUtterance(string:string)
        utterance.voice = voice
        synthesizer.speak(utterance)
    }
        override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
            vocImageView.image = UIImage(named: vocImage)
            English2.text = Englishlabel
            chinese2.text = chineselabel
            KK2.text = kklabel
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
